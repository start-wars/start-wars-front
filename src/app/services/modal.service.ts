import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  hideModal: boolean = true;

  constructor() { }

  openModal() {
    this.hideModal = false;
}

cerrarModal() {
  this.hideModal = true;
}

}
