import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const BASE_URL = environment.base_url


@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private _http: HttpClient) { }

  getPeople(nextPageUrl:string = ""):Observable<any>{

    if (!nextPageUrl){
      return this._http.get<any>(`${BASE_URL}/people`)
    } 

    return this._http.get<any>(`${nextPageUrl}`)
  }

  searchPeople(name:string):Observable<any>{
    return this._http.get<any>(`${BASE_URL}/people/?search=${name}`)
            .pipe( map( (res:any) => res.results))
  }
}
