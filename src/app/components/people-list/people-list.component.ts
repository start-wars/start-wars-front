import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { PeopleService } from 'src/app/services/people.service';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.sass']
})
export class PeopleListComponent implements OnInit {

  people:any[] = [] 
  character: any[] = []

  error:boolean = false
  search: string = ""
  loading: boolean = true
  orderValue:string = ""

  nextPageUrl:string = ""
  previousPageUrl: string = ""
  

  constructor(private peopleServ: PeopleService, public modalServ: ModalService) { }

  ngOnInit(): void {
    this.getPeople()
  }
  

  getPeople(nextPageUrl:string = ""){
    this.loading = true
    this.peopleServ.getPeople(nextPageUrl).subscribe(res => {

      this.nextPageUrl = res.next
      this.previousPageUrl = res.previous
      this.people = res.results

      if (this.orderValue) this.orderPeople(this.orderValue)

      this.loading = false

    }, err => console.log(err))
  }

  searchPeople(){
    this.loading = true
    this.peopleServ.searchPeople(this.search).subscribe( res => {
      this.people = res
      this.loading = false
    }, err => console.error(err))
  }


  onSelectPeople(p:any){
    this.character = p
    this.modalServ.hideModal = false
  
  }


  nextPage(){
    this.getPeople(this.nextPageUrl)
  }

  previousPage(){
    this.getPeople(this.previousPageUrl)
  }


  orderPeople(order_by:string ){

    switch (order_by) {
      case "name":
        this.people.sort(function(a, b){
          if(a.name < b.name) { return -1; }
          if(a.name > b.name) { return 1; }
          return 0;
       })
        break;

      case "mass":
        this.people.sort(function(a, b){
          return a.mass - b.mass;
        })
        break;
      
      case "height":
          this.people.sort(function(a, b){
            return a.height - b.height;
          })
          break;
    
      default:
        break;
    }

  }
}
